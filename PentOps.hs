{--
 - Copyright (c) 2023 Denis Remezov
 -
 - Permission to use, copy, modify, and/or distribute this software for any
 - purpose with or without fee is hereby granted, provided that the above
 - copyright notice and this permission notice appear in all copies.
 -
 - THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 - WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 - MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 - ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 - WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 - ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 - OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--}

--- Pentomino data structures and operations

module PentOps where

import Data.List

{-- Playing field matrix
    0:       empty cell;
    non-0:   piece code
--}
type Field = [[Int]]

{-- Piece matrix
    Each piece generates a group of rotations and inversions.
    Each piece within a group shares the same non-empty cell code.
    Empty cells are encoded as 0
--}
type Piece = [[Int]]

--- A set representing distinct pieces (no two elements belong to a symmetry group)
type Pieces = [Piece]

--- A piece group is a rotation/inversion symmetry group generated by a given piece
type PGroup = [Piece]
type PGroups = [PGroup]

--- Get an integer range [0 .. length xs - 1]
range :: [a] -> [Int]
range xs = [0 .. length xs - 1]

--- Skip i-th element
skip :: Int -> [a] -> [a]
skip i xs = take i xs ++ drop (i + 1) xs

belongs :: Eq a => a -> [a] -> Bool
belongs v = foldl (\b x -> b || x == v) False

--- Eliminate repeated items, preserving the order of elements remaining
unique :: Eq a => [a] -> [a]
unique = foldl (\ys x -> if belongs x ys then ys else ys ++ [x]) []

--- Invert a piece
invPiece :: Piece -> Piece
invPiece = map reverse

{-- Rotate a piece clockwise
    For any i = [0, height(src)-1], j = [0, width(src)-1]
    we have dst[j][height(src)-i-1] = src[i][j]
--}
rotatePiece :: Piece -> Piece
rotatePiece p = [[p !! (length p - i - 1) !! j | i <- range p] | j <- range (p !! 0)]

--- Generate a rotation/inversion symmetry group based an a piece
genPGroup :: Piece -> PGroup
genPGroup p =
    unique (inv_gr ++ rot1_gr ++ rot2_gr ++ rot3_gr)
    where
        inv_gr  = unique [p, invPiece p]
        rot1_gr = (map rotatePiece inv_gr)
        rot2_gr = (map rotatePiece rot1_gr)
        rot3_gr = (map rotatePiece rot2_gr)

{-- Check whether a piece is a valid fit at a specified position
    r:rs:   field
    p:      piece
    dy:     vertical offset
    dx:     horizontal offset
--}
validFit :: Field -> Piece -> Int -> Int -> Bool
validFit _ [] _ _           = True
validFit [] _ _ _           = False
validFit f@(r:rs) p@(r':rs') dy dx
    | dy + length p > length f  = False
    | dy > 0                = validFit rs p (dy - 1) dx
    | otherwise             = if valid_row_fit r r' dx then validFit rs rs' 0 dx
                              else False
    where
        valid_row_fit r r' dx
            | dx < 0                    = False
            | dx + length r' > length r = False
            | otherwise                 = all (== 0)
                                          [(r !! (j + dx)) * (r' !! j) | j <- range r']

{-- Add a piece to a field at a specified position
    No validity checks performed
    r:rs    field
    p:      piece
    dy:     vertical offset
    dx:     horizontal offset
--}
addPiece :: Field -> Piece -> Int -> Int -> Field
addPiece [] _ _ _           = []
addPiece rs [] _ _          = rs
addPiece (r:rs) p@(r':rs') dy dx
    | dy > 0                = [r] ++ addPiece rs p (dy - 1) dx
    | otherwise             = [comb_row r r'] ++ addPiece rs rs' 0 dx
    where
        comb_row r r' = [if j < dx || j >= dx + (length r') then r !! j
                         else let c' = r' !! (j - dx) in
                             if c' == 0 then r !! j
                             else c'
                         | j <- range r]

--- Return the minimal index of an element satisfying a predicate or length r if not found
minIdx :: [a] -> (a -> Bool) -> Int
minIdx [] _         = 0
minIdx (x:xs) pred
    | pred x        = 0
    | otherwise     = 1 + minIdx xs pred

--- Create an empty field, height x width
emptyField :: Int -> Int -> Field
emptyField h w  = replicate h (replicate w 0)

field :: Maybe Field -> Field
field (Just f)  = f

empty :: [a] -> Bool
empty []    = True
empty _     = False

{-- Attempt to fit a single piece at a specified position
    Return Nothing if no such fit is possible.
    Note: dy_p specification is an artefact of interactive APIs
    f:      field
    p:      piece
    dy_p:   vertical offset for the piece
    dx_p:   horizontal offset for the piece
--}
fitPiece :: Field -> Piece -> Int -> Int -> Maybe Field
fitPiece f p dy_p dx_p
    | validFit f p dy_p dx_p == False  = Nothing
    | otherwise                         = Just (addPiece f p dy_p dx_p)

{-- Attempt a recursive fit beginning with an element of a group at the first row
    to fill a specified cell
    f:      field
    dx:     horizontal offset of the first empty cell to fill
    pg:     pieces to choose from
    pgs:    remaining piece groups to fit afterwards
--}
autoFitGroup :: Field -> Int -> PGroup -> PGroups -> Maybe Field
autoFitGroup _ _ [] _           = Nothing
autoFitGroup f dx pg@(p:ps) pgs
    | mf1 == Nothing            = autoFitGroup f dx ps pgs          -- try next piece
    | otherwise                 =                                   -- piece fits, recur for remaining groups
        if mf2 == Nothing then autoFitGroup f dx ps pgs             -- the rest failed, try next piece
        else mf2
    where
        mf1 = fitPiece f p 0 (dx - (minIdx (p !! 0) (/= 0)))
        mf2 = fillMinRow [] (field mf1) pgs

{-- Attempt a recursive fit beginning with the piece group at a given index,
    then loop to the next index
    f:      field
    dx:     horizontal offset of the first empty cell to fill
    pgs:    groups to find a fit amongst
    i_grp:  current group to try
--}
autoFitGroups :: Field -> Int -> PGroups -> Int -> Maybe Field
autoFitGroups f dx pgs i_grp
    | i_grp >= length pgs       = Nothing
    | otherwise                 =
        if mf1 == Nothing then autoFitGroups f dx pgs (i_grp + 1)    -- try next group
        else mf1
    where mf1 = autoFitGroup f dx (pgs !! i_grp) (skip i_grp pgs)

{-- Autofit recursively beginning with a piece starting at the first row of the incomplete
    part of the field at the lowest dx offset
    f_c:    completed rows
    f:      rows with possibly empty cells
    pgs:    remaining piece groups to fit
 --}
fillMinRow :: Field -> Field -> PGroups -> Maybe Field
fillMinRow f_c f []         = Just (f_c ++ f)                   -- done: no more pieces left
fillMinRow f_c f@(r:rs) pgs
    | dx >= length r        = fillMinRow (f_c ++ [r]) rs pgs    -- r has been filled as well, move down
    | otherwise             =                                   -- an empty cell exists: must fill this row
        if mf1 == Nothing then Nothing
        else Just (f_c ++ (field mf1))
    where
        dx = minIdx r (== 0)
        mf1 = autoFitGroups f dx pgs 0         -- limit deeper calls to the field subset of yet incomplete rows

{-- Auto fit a set of pieces to a field.  Pieces can be inverted and rotated.
    On success, the resulting field is Just Field, else Nothing.
    f:      field
    ps:     remaining pieces to fit

    Algorithm overview:
        - Generate a symmetry group for each piece
            - Fit a piece for the minimal row with an empty cell at the minimal dx offset:
              Loop over groups
                Loop over group elements; accept criteria: piece fits and no segment voids

    XXX do all combinations, eliminating symmetries
 --}
autoFit :: Field -> Pieces -> Maybe Field
autoFit f ps
    -- Optimization: most fields a column-wide; transpose for better performance
    | mf1 == Nothing        = Nothing
    | otherwise             = Just (transpose (field mf1))
    where mf1 = fillMinRow [] (transpose f) (map genPGroup ps)
