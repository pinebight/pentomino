{--
 - Copyright (c) 2023 Denis Remezov
 -
 - Permission to use, copy, modify, and/or distribute this software for any
 - purpose with or without fee is hereby granted, provided that the above
 - copyright notice and this permission notice appear in all copies.
 -
 - THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 - WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 - MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 - ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 - WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 - ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 - OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--}

{-- Pentomino game with an automatic solver option.
    Just toying with Haskell, nothing serious.
--}

import PentOps
import Data.Char
import System.IO
import System.Timeout

clrscr :: IO ()
clrscr = putStr "\ESC[2J"

--- Piece definitions
p0 = [[0, 1, 0],
      [1, 1, 1],
      [0, 1, 0]]

p1 = [[2, 2, 2],
      [0, 2, 2]]

p2 = [[0, 3, 0, 0],
      [3, 3, 3, 3]]

p3 = [[4, 4, 0, 0],
      [0, 4, 4, 4]]

p4 = [[5, 5, 5, 5, 5]]

p5 = [[0, 0, 6],
      [0, 6, 6],
      [6, 6, 0]]

p6 = [[7, 0, 7],
      [7, 7, 7]]

p7 = [[8, 8, 0],
      [0, 8, 0],
      [0, 8, 8]]

p8 = [[9, 0, 0],
      [9, 0, 0],
      [9, 9, 9]]

p9 = [[10, 10, 10],
      [ 0, 10,  0],
      [ 0, 10,  0]]

p10 = [[11, 11, 11, 11],
       [ 0,  0,  0, 11]]

p11 = [[12, 12,  0],
       [ 0, 12, 12],
       [ 0, 12,  0]]

--- Piece code to display character LUT 
val_ch_lut = [
    (0,  '\183'),   -- middle dot (U+00B7)
    (1,  '+'),
    (2,  'x'),
    (3,  'o'),
    (4,  's'),
    (5,  '='),
    (6,  'w'),
    (7,  'u'),
    (8,  'z'),
    (9,  'L'),
    (10, 'T'),
    (11, 'I'),
    (12, 'G')]

mapSym :: Int -> Char
mapSym v = head [snd p | p <- val_ch_lut, fst p == v]

showSym :: Int -> IO ()
showSym v = putChar (mapSym v)

--- As showSym but with transparent background
showSymNoBg :: Int -> IO ()
showSymNoBg v = putChar (if v == 0 then ' ' else mapSym v)

--- Field row or column index (least significant digit only for brevity)
rowcolIndex :: Int -> String
rowcolIndex i = show (i `mod` 10)

{-- Display one row of a field
    i_r:     row index
    r:       field row
--}
showRow :: Int -> [Int] -> IO ()
showRow i_r r  = do
    putStr (rowcolIndex i_r)
    putStr "   "
    sequence_[do putChar ' '; showSym c | c <- r]
    putChar '\n'

--- Display a field
showField :: Field -> IO ()
showField f = do
    putStr "    "
    sequence_ [do putChar ' '; putStr (rowcolIndex i) | i <- range (f !! 0)]
    putStr "\n\n"
    sequence_ [showRow i (f !! i) | i <- range f]

status_disp_time_us = 2 * 1000 * 1000

showStatus :: String -> IO ()
showStatus msg  = do
    putStrLn msg
    sequence_ [putChar '\n' | _ <- [1 .. 5]]    -- match the space for an alternative piece display

--- Display a piece
showPiece :: Piece -> IO ()
showPiece p = do
    putChar '\n'
    -- Centre the piece both verticall and horizontally
    let n_blank_rows = 5 - (length p)
    let n_blank_rows_above = n_blank_rows `div` 2
    -- As a visual hack we put a space between horizontal chracters,
    -- hence the total horizontal padding would be 2*max_w - 2*w
    -- effective left padding is (2*max_w - 2*w) / 2 = max_x - w
    -- the common (for all pieces) offset is adjusted for visual effect
    let n_blank_cols_left = 9 - (length (p !! 0))
    show_blank_rows n_blank_rows_above
    sequence_ [show_row (p !! i) n_blank_cols_left | i <- range p]
    show_blank_rows (n_blank_rows - n_blank_rows_above)
    where
        show_blank_rows n = sequence_ [putChar '\n' | _ <- [1 .. n]]
        show_row r n_left_padding = do
            sequence_ [do putChar ' ' | _ <- [1 .. n_left_padding]]
            sequence_ [do putChar ' '; showSymNoBg c | c <- r]
            putChar '\n'

{-- Display caption and extra info
    f:      field
    ps:     pieces still available
    disp_f: function to display additional information
--}
showLayout :: Field -> Pieces -> IO () -> IO ()
showLayout f ps disp_f = do
    clrscr
    putStr "h - Help  p - Place Piece  a - Autoplay  s - Set Size  r - Reset  q -Quit\n\n"
    let n_pieces = length ps
    let ps_msg = if n_pieces == 0 then "Game complete"
                 else "Pieces left: " ++ show n_pieces
    putStrLn ps_msg
    disp_f

    putChar '\n'
    showField f
    putStr "\n\n"

--- Get a non-negative integer; -1 on error
toPInt :: String -> Int
toPInt ""               = -1
toPInt s
    | all isDigit s     = read s
    | otherwise         = -1

--- Prompt for an index value entry
--- return -1 on error
getIdx :: String -> (Int -> Bool) -> IO Int
getIdx prompt pred = do
    putStr (prompt ++ "> ")
    s <- getLine
    let idx = toPInt s
    return (if idx >= 0 && (pred idx) then idx else -1)

--- Replace i-th element xs[i] with f(xs[i])
replace :: Int -> (a -> a) -> [a] -> [a]
replace i f xs = take i xs ++ (f (xs !! i)) : drop (i + 1) xs

{-- Interactively select a new piece to add to the field
    f:      field
    ps:     pieces available
    i_sel:  index of the current selection
    return (piece selected, pieces remaining)
--}
selectPiece :: Field -> Pieces -> Int -> IO (Piece, Pieces)
selectPiece _ [] _      = return ([], [])
selectPiece f ps i_sel  = do
    showLayout f ps (showPiece (ps !! i_sel))
    putStr "Piece (n - next  b - previous  r - rotate  f - flip  <ENTER> - select) > "
    cmd <- getChar
    putChar '\n'
    case cmd of
        'n'     -> selectPiece f ps (i_new 1)
        'b'     -> selectPiece f ps (i_new (-1))
        'r'     -> selectPiece f (replace i_sel rotatePiece ps) i_sel
        'f'     -> selectPiece f (replace i_sel invPiece ps) i_sel
        '\n'    -> return (ps !! i_sel, skip i_sel ps)
        _       -> selectPiece f ps i_sel
    where i_new di = (i_sel + di) `mod` (length ps)

placePiece :: Field -> Pieces -> IO ()
placePiece f ps = do
    (piece, ps1) <- selectPiece f ps 0
    if piece == [] then
        repl "No pieces left" f ps
    else do
        showLayout f ps (showPiece piece)
        i_row <- getIdx "Row" (< length f)
        if i_row < 0 then
            repl "Invalid row index" f ps
        else do
            showLayout f ps (showPiece piece)
            i_col <- getIdx "Column" (< length (f !! 0))
            if i_col < 0 then
                repl "Invalid column index" f ps
            else do
                let mf1 = fitPiece f piece i_row i_col
                if mf1 == Nothing then
                    repl "Unable to fit" f ps
                else do
                    repl "" (field mf1) ps1

setSize :: Field -> Pieces -> IO ()
setSize f ps = do
    showLayout f ps (showStatus "")
    h <- getIdx "Field height" (\x -> any (x ==) [3, 4, 5, 6])
    if h < 0 then
        repl "Invalid field height" f ps
    else do
        let w = 60 `div` h
        let msg = "New field dimensions: " ++ (show h) ++ "X" ++ (show w)
        let f1 = emptyField h w
        repl msg f1 ps0

autoPlay :: Field -> Pieces -> IO ()
autoPlay f ps = do
    showLayout f ps (showStatus "")
    let mf1 = autoFit f ps
    if mf1 == Nothing then repl "Unable to fit all pieces" f ps
    else repl "" (field mf1) []

--- The full piece set
ps0 = [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11]

--- Main loop
repl :: String -> Field -> Pieces -> IO ()
repl status f ps = do
    showLayout f ps (showStatus status)
    putStr "Command> "
    cmd <- timeout (if status == "" then maxBound :: Int else status_disp_time_us) getChar
    putChar '\n'
    case cmd of
        Nothing     -> repl "" f ps
        (Just 'q')  -> return ()
        (Just 's')  -> setSize f ps
        (Just 'r')  -> repl "" (emptyField (length f) (length (f !! 0))) ps0
        (Just 'h')  -> repl "Not yet implemented" f ps
        (Just 'p')  -> placePiece f ps
        (Just 'a')  -> autoPlay f ps
        (Just x)    -> repl "Invalid command" f ps

main :: IO ()
main  = do
    let h = 3
    let w = 20
    let f0 = emptyField h w

    hSetBuffering stdout NoBuffering
    hSetBuffering stdin NoBuffering

    repl "" f0 ps0
